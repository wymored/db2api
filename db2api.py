from bottle import Bottle, run, request
import dal

import json



config = open('connections.json').read()

'''
connections = {}
for connection in config.sections():
    if connection['driver'] == 'mysql':        
        connections[connection] = dal.mysql(user=connection['user'],password=connection['password'], host=connection['host'], database=connection['database'])

'''     


app = Bottle()
servers = json.loads(config)
db = dal.connect(servers)
@app.route('/')
def index():

    r = ''
    for d in servers:
        r+='<a href="%s%s">%s </a><br>' % (request.url,d,d)
    return '''<html>
    <body>
    Available servers:<br>
    %s
    <a href=api > Link to json api</a>
    </body><html>
    ''' % r


@app.route('/<server>')
def databases(server=''):
    r = ''

    for d in db.databases(server):
        r+='<a href="%s/%s">%s </a><br>' % (request.url,server,d)
    return '''<html>
    <body>
    Databases in server %s:<br>
    %s
    <a href="api/%s" > Link to json api</a>
    </body><html>
    ''' % (server,r,server)

@app.route('/<server>/<database>')
def tables(server='',database=''):
    r = ''

    for t in db.tables(server,database):
        r+='<a href="http://%s/%s/%s/%s"> %s </a><br>' % (request.urlparts[1],server,database,t,t)
    return '''<html>
    <body>
    Tables in database %s:<br>
    %s
    <a href="http://%s/api/%s/%s" > Link to json api</a>
    </body><html>
    ''' % (server,r,request.urlparts[1],server,database)


@app.route('/<server>/<database>/<table>')
def table(server='',database='',table=''):
    r = '<table>'
    r += '<tr>'
    t = db.table(server,database, table)
    for col in t['cols']:
        r += '<th> %s </th>' % col[0]
    r += '</tr>'
    for row in t['rows']:
        r += '<tr>'
        for cell in row:
            r += '<td> %s </td>' % cell
        r+= '</tr>'
    r += '</table>'
    return '''<html>
    <body>
    Table %s:<br>
    %s
    <a href="http://%s/api/%s/%s/%s" > Link to json api</a>
    </body><html>
    ''' % (table,r,request.urlparts[1],server,database,table)

@app.route('/api')
def databases():
    r = dict()
    for d in dbs.databases():
        r[d]=request.url+d
    return str(r)

run(app, host='localhost', port=8080,reloader=True,debug=True)