# dal - a database abstraction layer for db2api



class mysql:
    
    def __init__(self, user, password, host, database=''):
        import pymysql
        conn = pymysql.connect(user=user, passwd=password, host=host, db=database)
        self.db = conn.cursor()

    def databases(self):
        self.db.execute("show databases")
        return self.db.fetchall()
    def tables(self,database):
        self.db.execute("show tables from %s" % database)
        return self.db.fetchall()
    def columns(self,table):
        self.db.execute("show columns from %s " % (table))
        return self.db.fetchall()
    def query(self,query, limit=None):
        if limit:
            self.db.execute('%s limit %s ' % (query, limit))
        else:
            self.db.execute(query)
        return self.db.fetchall()
    def execute(self,query):
        sql = "start transaction; %s; commit;" % (query)
        return self.db.execute(sql)
    def print(self):
        for row in self.db.fetchall():
            print(row)  

#m = mysql()
#m.tables()
#print(m.query("alter table icms_conselho add numero_registro int(11)"))
#print(m.columns('icms_conselho'))
#print(m.query("select * from icms_conselho"))

#print (m.columns('icms_conselheiro'))



class postgresql:

    def __init__(self,host,user,password,database=''):
        from pg8000 import DBAPI
        conn = DBAPI.connect(host=host, user=user, password=password, database=database)
        
        self.db = conn.cursor()

    def databases(self):
        self.db.execute("SELECT datname FROM pg_database WHERE datistemplate = false;")
        return self.db.fetchall()
    def tables(self):
        self.db.execute("SELECT * FROM information_schema.tables WHERE table_type = 'BASE TABLE' AND table_schema = 'public' ")     
        return self.db.fetchall()
    def columns(self,table):
        self.db.execute(" SELECT  column_name  FROM information_schema.columns where table_name = '"+table+"' ")
        return self.db.fetchall()
    def query(self,query):
        self.db.execute(query)
        return self.db.fetchall()
    def print(self):
        for row in self.db.fetchall():
            print(row)

class connect():
    def __init__(self,servers):
        self.servers = servers
    def connect(self,server, database=''):
        srv = self.servers[server]
        if srv['driver'] == 'mysql':
            self.db = mysql(host=srv['host'], user=srv['user'], password=srv['password'], database=database or srv['database'])
        elif srv['driver'] == 'postgresql':
            self.db = postgresql(host=srv['host'], user=srv['user'], password=srv['password'], database=srv['database'])
    def databases(self,server):
        self.connect(server)
        db = self.db
        rows = db.databases()
        dbs = []
        for r in rows:
            if r[0] not in self.servers[server]['exceptions']['databases']:
                dbs.append(r[0])
        return dbs
    def tables(self,server,database):
        self.connect(server)
        db = self.db
        rows = db.tables(database)
        dbs = []
        for r in rows:
            if r[0] not in self.servers[server]['exceptions']['tables']:
                dbs.append(r[0])
        return dbs
    def table(self,server,database,table):
        self.connect(server, database)
        db = self.db
        cols = db.columns(table)
        rows = db.query('select * from %s' % table, limit = '100')
        return dict(cols=cols, rows=rows)

#pg = postgresql()